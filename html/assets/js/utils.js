/* globals noUiSlider */
$(document).ready(function() {
    if (window.location.pathname.indexOf('view-') < 0) {
        $('body').css('padding-top', 0);
    }

    $(document).trigger('enhance');

    $('[data-autoscroll], [data-toggle="collapse"], [data-toggle="collapse-layer"], [data-toggle="header"], [data-toggle="dropdown"], .header__dropdown__close, .filter-sidemenu__close, .js-modal').on('click', function(e) {
        e.preventDefault();
    });

    $('.nav__link').on('click', function(e) {
        if ($(this).parent().find('> .nav__sub-container').length > 0) {
            e.preventDefault();
        }
    });

    var $search = $('[data-suggestion-list]');

    $search.on('keydown', function(event) {
        if (event.which != 27 && event.which != 9) {
            $(this).teliaSuggestion('show');
        }
    });

    // Video Rental infinite scroll demo
    function addNewVideoRentalContent() {
        // start loader function

        // ajax here
        var randomInt = Math.random().toString(36).substr(7);
        var data = '<div class="grid__col--xs-6 grid__col--sm-4 grid__col--md-3 collapse-layer">' +

        '<a href="#movie-' + randomInt + '" class="text text-color display-block" data-toggle="collapse-layer" data-parent="#movieRentalCollapse" aria-expanded="false">' +
        '<img src="data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==" data-src="https://images-na.ssl-images-amazon.com/images/M/MV5BNGRkMTllZTUtZTQyYi00NjVlLTlhZjEtODExNjQ4YjQ1Y2RjXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_UY268_CR0,0,182,268_AL_.jpg" alt="Movie-' + randomInt + '" class="img-responsive center-block lazyload" />' +
        '<p class="text-bold text-center">Family Guy</p>' +
        '</a>' +
        '<div id="movie-' + randomInt + '" class="collapse">' +
        '<div class="collapse__container">' +
        '<div class="grid">' +
        '<div class="grid__col--md-4 grid__col--lg-3">' +
        '<img data-src="https://images-na.ssl-images-amazon.com/images/M/MV5BNGRkMTllZTUtZTQyYi00NjVlLTlhZjEtODExNjQ4YjQ1Y2RjXkEyXkFqcGdeQXVyNTA4NzY1MzY@._V1_UY268_CR0,0,182,268_AL_.jpg" alt="Movie-#" class="hidden--xs hidden--sm img-responsive lazyload center-block">' +
        '</div>' +
        '<div class="grid__col--md-8 grid__col--lg-9">' +
        '<div class="text">' +
        '<div class="grid">' +
        '<div class="grid__col--sm-12">' +
        '<h3>Дабл трабл</h3>' +
        '<h4>Family Guy</h4>' +
        '</div>' +
        '</div>' +
        '<div class="grid">' +
        '<div class="grid__col--sm-5 grid__col--md-3">' +
        '<p class="text-price">SD hind: 2.2 €</p>' +
        '</div>' +
        '<div class="grid__col--sm-5 grid__col--md-3">' +
        '<p class="text-price">HD hind: 3.2 €</p>' +
        '</div>' +
        '</div>' +
        '<p>Vene komöödia. Roman on ilus, edukas ja vallaline raadiosaatejuht. Ta annab oma saatesse helistavatele inimestele sageli nõu. Ühel päeval helistab raadiosse tütarlaps, kes uurib, kas ta peaks oma isaga ühendust võtma, kellega nad kunagi kohtunud ei ole. Roman soovitab kindlasti võtta… ja õhtul ongi nooruke nägus Aljona ta ukse taga. Kuid Romani tütar ei saabu üksinda – vanaisaga tutvuma tulnud Aljona pisipoeg osutub tõeliseks nuhtluseks, kes kogu mehikese senise mugava elu totaalselt pea peale pöörab…</p>' +
        '<p>' +
        'Režissöör: <strong>Eduard Oganesyan</strong>' +
        '<br>' +
        'Peaosades: <strong>Daniil Belykh, Ekaterina Varnava, Lera Kulikova, Slava jt.</strong>' +
        '</p>' +
        '<div class="grid">' +
        '<div class="grid__col--sm-4">Aasta: <strong>2007</strong></div>' +
        '<div class="grid__col--sm-4">Filmi pikkus: <strong>1:23</strong></div>' +
        '<div class="grid__col--sm-4">Audio: <strong>ENG</strong></div>' +
        '<div class="grid__col--sm-4">Subtiitrid: <strong>EST</strong></div>' +
        '<div class="grid__col--sm-4">Video: <strong>SD</strong></div>' +
        '</div>' +
        '<p>' +
        '<a href="https://www.youtube.com/results?search_query=Simpsons+Movie%2C+The+trailer" class="btn btn--link" target="_blank">' +
        '<strong>Vaata filmi treilerit </strong>' +
        '<svg class="icon">' +
        '<use xlink:href="' + telia.svg_path + '#arrow-right"></use>' +
        '</svg>' +
        '</a>' +
        '</p>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

        $('#movieCategoryTarget').append(data);

        $('[data-toggle="collapse-layer"]').on('click', function(e) {
            e.preventDefault();
        });

        // end loader function
    };

    $(window).on('scroll', function() {
        var winTop = $(window).scrollTop();
        var docHeight = $(document).height();
        var winHeight = $(window).height();
        var scrollTrigger = 0.95;

        if ((winTop / (docHeight - winHeight)) > scrollTrigger) {
            addNewVideoRentalContent();
        }
    });

    $('#callback-id1').collapse({
        loadContent: function(e, callback) {
            setTimeout(function() {
                // returns data, when ready
                callback('<p>Hello AJAX</p>');
            }, 3000);
        },
        toggle: false
    });

    var $collapseModal = $('#searchResultsCollapse');

    $collapseModal.teliaCollapseModal({
        trigger: $('.search-large')
    });

    $('.search-large__submit').on('click', function() {
        $collapseModal.teliaCollapseModal('show');
    });
});
